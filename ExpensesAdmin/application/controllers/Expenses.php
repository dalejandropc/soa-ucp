<?php
if (! defined('BASEPATH')) exit('No direct script access allowed');

class Expenses extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('Expenses_mod');
		$this->load->helper('url');
	}

	public function index()
	{
		if(isset($_GET['lang']))
			$_SESSION['language'] = $_GET['lang'];
		if(!isset($_SESSION['language']))
			$_SESSION['language'] = 'english';
		$this->lang->load('iheader', $_SESSION['language']);
    	$this->lang->load('expenses', $_SESSION['language']);
		$this->lang->load('footer', $_SESSION['language']);
		$this->load->helper('url');
		$this->load->view('head');
		if(isset($_SESSION['idperson'])){
			$data = array(
				'datatable' => $this->Expenses_mod->view_expenses()
			);
			$this->load->view('Expenses_view',$data);
		}else{
			$this->load->view('default');
		}
		$this->load->view('footer');
	}
	public function getTypeExpenses()
	{
		if(isset($_GET['lang']))
			$_SESSION['language'] = $_GET['lang'];
		if(!isset($_SESSION['language']))
			$_SESSION['language'] = 'english';
		$this->lang->load('iheader', $_SESSION['language']);
    	$this->lang->load('expensestype', $_SESSION['language']);
		$this->lang->load('footer', $_SESSION['language']);
		$this->load->helper('url');
		$this->load->view('head');
		if(isset($_SESSION['idperson'])){
			$data = array(
				'datatable' => $this->Expenses_mod->view_type_expenses()
			);
			$this->load->view('ExpensesType_view',$data);
		}else{
			$this->load->view('default');
		}
		$this->load->view('footer');
	}
	function newTypeExpense(){
		if(isset($_GET['lang']))
			$_SESSION['language'] = $_GET['lang'];
		if(!isset($_SESSION['language']))
			$_SESSION['language'] = 'english';
		$this->lang->load('iheader', $_SESSION['language']);
    	$this->lang->load('expensesnewtype', $_SESSION['language']);
		$this->lang->load('footer', $_SESSION['language']);
		$this->load->helper('url');
		$this->load->view('head');
		if(isset($_SESSION['idperson'])){
			$this->load->view('ExpensesSaveType_view');
		}else{
			$this->load->view('default');
		}
		$this->load->view('footer');
	}
	function saveTypeExpense(){
		$result = $this->Expenses_mod->newTypeExpense($_POST['nameus'], $_POST['namept'], $_POST['namesp']);
		if($result = 0)
			$this->session->set_flashdata('error','Error');
		elseif($result = 1)
			$this->session->set_flashdata('ok','Ok');
		redirect('Expenses/newTypeExpense');
	}
	//function expenses(){ 
	function newExpense(){ 
		if($this->input->is_ajax_request())
		{
			$Expenses = $this->Expenses_mod->view_expenses();
			echo $Expenses;
			echo json_encode($Expenses);
		}
		$Expenses = $this->Expenses_mod->newexpense('1', '1', '1235');
			echo $Expenses;
			echo json_encode($Expenses);
	}
}