<?php
if (! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('Expenses_mod');
		$this->load->helper('url');
	}
	function index(){
		if(isset($_POST['username']))
			$Expenses = $this->Expenses_mod->login($_POST['username'], $_POST['password']);
		
		if(isset($_GET['lang']))
			$_SESSION['language'] = $_GET['lang'];
		if(!isset($_SESSION['language']))
			$_SESSION['language'] = 'english';
		$this->lang->load('iheader', $_SESSION['language']);
    	$this->lang->load('default', $_SESSION['language']);
		$this->lang->load('footer', $_SESSION['language']);
		$this->load->helper('url');
		$this->load->view('head');
		$this->load->view('default');
		$this->load->view('footer');
	}
	function logout(){
		session_destroy();
		$_SESSION = array();
		
		if(isset($_GET['lang']))
			$_SESSION['language'] = $_GET['lang'];
		if(!isset($_SESSION['language']))
			$_SESSION['language'] = 'english';
		$this->lang->load('iheader', $_SESSION['language']);
    	$this->lang->load('default', $_SESSION['language']);
		$this->lang->load('footer', $_SESSION['language']);
		$this->load->helper('url');
		$this->load->view('head');
		$this->load->view('default');
		$this->load->view('footer');
	}
}