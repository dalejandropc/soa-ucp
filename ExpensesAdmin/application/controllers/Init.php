<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Init extends CI_Controller {
	public function index()
	{
		
		if(isset($_GET['lang']))
			$_SESSION['language'] = $_GET['lang'];
		if(!isset($_SESSION['language']))
			$_SESSION['language'] = 'english';
		$this->lang->load('iheader', $_SESSION['language']);
    	$this->lang->load('default', $_SESSION['language']);
		$this->lang->load('footer', $_SESSION['language']);
		$this->load->helper('url');
		$this->load->view('head');
		$this->load->view('default');
		$this->load->view('footer');
	}
}
