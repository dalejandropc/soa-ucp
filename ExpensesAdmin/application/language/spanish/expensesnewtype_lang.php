<?php
	$lang['ntyp_tit'] = 'NUEVO TIPO DE GASTO';
	$lang['ntyp_stit'] = 'Crear un nuevo tipo de gasto';
	$lang['ntyp_ok'] = 'El nuevo tipo se ha creado satisfactoriamente';
	$lang['ntyp_error'] = 'Error, No se realizarón cambios';
	$lang['ntyp_nameus'] = 'NOMBRE DE TIPO DE GASTO EN INGLES';
	$lang['ntyp_namept'] = 'NOMBRE DE TIPO DE GASTO EN PORTUGUÉS';
	$lang['ntyp_namesp'] = 'NOMBRE DE TIPO DE GASTO EN ESPAÑOL';
	$lang['ntyp_but'] = 'GUARDAR';
?>