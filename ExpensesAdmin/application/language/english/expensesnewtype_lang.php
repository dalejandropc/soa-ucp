<?php
	$lang['ntyp_tit'] = 'NEW EXPENSE TYPE';
	$lang['ntyp_stit'] = 'Create a new Type Expense';
	$lang['ntyp_ok'] = 'New type expense successfull';
	$lang['ntyp_error'] = 'Error, not successfull';
	$lang['ntyp_nameus'] = 'ENGLISH NAME TYPE EXPENSE';
	$lang['ntyp_namept'] = 'PORTUGUESE NAME TYPE EXPENSE';
	$lang['ntyp_namesp'] = 'SPANISH NAME TYPE EXPENSE';
	$lang['ntyp_but'] = 'SAVE';
?>