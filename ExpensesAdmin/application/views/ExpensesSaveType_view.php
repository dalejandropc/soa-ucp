
<!--============== Contact us ==============-->

<div class="container" id="contact">
  <div class="row PageHead">
    <div class="col-md-12">
      <h1><?=$this->lang->line('ntyp_tit');?></h1>
      <h3><?=$this->lang->line('ntyp_stit');?></h3>
    </div>
  </div>
  <div class="row ContactUs">
    <div class="col-md-3">
    </div>
    <div class="col-md-6">
      <form class="form" id="register" action="<?= base_url() ?>Expenses/saveTypeExpense" method="post">
        <?php if ($this->session->flashdata('ok')): ?>
            <div class="alert alert-submit" role="submit">       
                <button class="close" data-dismiss="submit"><span>&times;</span></button>
                <strong></strong><?=$this->lang->line('ntyp_ok');?> 
            </div> 
        <?php endif ?>
        <?php if ($this->session->flashdata('error')): ?>
            <div class="alert alert-danger" role="alert">       
                <button class="close" data-dismiss="alert"><span>&times;</span></button>
                <strong></strong><?=$this->lang->line('ntyp_error');?> 
            </div> 
        <?php endif ?>
        <div class="form-group">
          <input class="form-control" type="text" placeholder="<?=$this->lang->line('ntyp_nameus');?>" name="nameus" id="nameus" value="<?php if(isset($nameus))echo $nameus;?>">
        </div>
        <div class="form-group">
          <input class="form-control" type="text" placeholder="<?=$this->lang->line('ntyp_namept');?>" name="namept" id="namept" value="<?php if(isset($namept))echo $namept;?>">
        </div>
        <div class="form-group">
          <input class="form-control" type="text" placeholder="<?=$this->lang->line('ntyp_namesp');?>" name="namesp" id="namesp" value="<?php if(isset($namesp))echo $namesp;?>">
        </div>
        <div class="form-group">
          <p>
          <button name="btnGuardar" type="submit" class="btn btn-success btn-lg"><?=$this->lang->line('ntyp_but');?></button>
          </p>
          <span class="loading"></span> </div>          
      </form>
    </div>
  </div>
</div>