 <div class="panel-body table-responsive">                      
                <table class="table table-striped table-bordered table-hover" id="dataTables-example">   
                    <thead>
                        <tr>                                
                            <th><?= $this->lang->line('expt_id')?></th>
                            <th><?= $this->lang->line('expt_name')?></th>                       
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (isset($datatable)): ?>

                            <?php foreach ($datatable as $data): ?>
                                <tr>                                        
                                    <td><?= $data->id ?></td> 
                                    <td><?= $data->namesp ?></td>                                            
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                </table>                   
            </div><!-- /.box-body -->