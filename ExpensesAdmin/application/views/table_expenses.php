 <div class="panel-body table-responsive">                      
                <table class="table table-striped table-bordered table-hover" id="dataTables-example">   
                    <thead>
                        <tr>                                
                            <th><?= $this->lang->line('exp_email')?></th>
                            <th><?= $this->lang->line('exp_fulname')?></th>
                            <th><?= $this->lang->line('exp_name')?></th>                            
                            <th><?= $this->lang->line('exp_amount')?></th>                          
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (isset($datatable)): ?>

                            <?php foreach ($datatable as $data): ?>
                                <tr>                                        
                                    <td><?= $data->email ?></td>  
                                    <td><?= $data->fullname ?></td>
                                    <td><?= $data->namesp ?></td>
                                    <td><?= $data->amount ?></td>                                              
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                </table>                   
            </div><!-- /.box-body -->