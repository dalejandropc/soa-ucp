<!--============== Footer ==============-->

<div class="footer">
  <div class="container">
    <div class="row footerlinks">
      <div class="col-sm-4 col-md-2">
        <p>EMAIL US</p>
        <ul>
          <li><a href="mailto:dalejandropc@gmail.com" target="_blank">support@</a></li>
          <li></li>
        </ul>
      </div>
    </div>
    <div class="row copyright">
      <p>Copyright &copy; 2014. dalejandropc</p>
    </div>
  </div>
</div>

<!--==============MODAL LOGIN=================--> 

<!-- Modal -->
<div class="modal fade LoginSignup" id="Login" tabindex="-1" role="dialog" aria-labelledby="LoginLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 class="modal-title"><?=$this->lang->line('foot_login');?></h3>
      </div>
      <div class="modal-body">
        <form method="post" action="<?=base_url('Login')?>">
          <div class="form-group">
            <input class="form-control input-lg" type="text" name="username" size="50" placeholder="<?=$this->lang->line('foot_mail');?>"/>
          </div>
          <div class="form-group">
            <input class="form-control input-lg" type="password" name="password" size="20" placeholder="<?=$this->lang->line('foot_password');?>"/>
          </div>
          <div class="form-group">
            <input type="submit" value="<?=$this->lang->line('foot_account');?>" class="btn btn-success btn-lg"/>
          </div>
          <div class="form-group">
            <a href="<?=base_url('Login/recovery')?>"><?=$this->lang->line('foot_rec');?></a>
          </div>
        </form>
      </div>
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>
<!-- /.modal --> 

<!--==============BOOTSTRAP JS=================--> 

<script src="<?php echo base_url("../assets/js/bootstrap.min.js"); ?>"></script>
</body>
</html>