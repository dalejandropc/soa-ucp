<!--==============HEADER =================-->
<div class="jumbotron masthead">
  <div class="container"> 
  <?php if ($this->session->flashdata('error')){ ?>
    <div class='alert alert-success alert-dismissable'>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
      <strong><?=$this->session->flashdata('error'); ?></strong>
    </div>
  <?php } ?>
  <?php if ($this->session->flashdata('contact')){ ?>
    <div class='alert alert-success alert-dismissable'>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
      <strong><?=$this->session->flashdata('contact'); ?></strong>
    </div>
  <?php } ?>
    
    <!--==============Hero Unit=================-->
    
    <div class="flexslider">
      <ul class="slides">
       <!-- <li>
          <div class="slide3">
            <p class="pull-left"><img src="<?php echo base_url("../assets/images/server2.png"); ?>" alt="server" class="img-responsive"></p>
            <h1><?=$this->lang->line('index_start');?></h1>
            <h4><?=$this->lang->line('index_join');?></h4>
          </div>
        </li>

       <li>
          <div class="hero-unit">
            <h2><?=$this->lang->line('index_question');?></h2>
            <h4><?=$this->lang->line('index_explan');?></h4>
            <div class="slider-actions text-center"> <a href="<?php echo base_url("Links/features "); ?>" class="btn btn-success btn-lg"><?=$this->lang->line('index_butfeat');?></a> <a href="<?php echo base_url("Links/pricing "); ?>" class="btn btn-primary btn-lg"><?=$this->lang->line('index_butprici');?></a> </div>
          </div>
        </li>
        <li>
          <div class="slide2">
            <p><img src="<?php echo base_url("../assets/images/server1.png"); ?>" alt="server" class="img-responsive center-block"></p>
            <h3><?=$this->lang->line('index_soon');?></h3>
          </div>
        </li>-->
      </ul>
    </div>
  </div>
</div>

<!--==============Content Area=================-->

<div class="container"> 
  <!--============== Main Features ==============-->
  
 

</div>
<!--Container Closed--> 
