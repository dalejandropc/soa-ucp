<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>My Expenses</title>
<link rel="icon" type="image/png" href="<?php echo base_url("../assets/images/web2.png"); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--RECAPTCHA GOOGLE-->
<!--<script src='https://www.google.com/recaptcha/api.js'></script>-->
<!-- Bootstrap -->
<link href="<?php echo base_url("../assets/css/bootstrap.min.css"); ?>" rel="stylesheet">

<!--==============GOOGLE FONT - OPEN SANS=================-->
<link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">

<!--============== ICON FONT FONT-AWESOME=================-->

<link href="<?php echo base_url("../assets/css/font-awesome.css"); ?>" rel="stylesheet">

<!--==============MAIN CSS FOR HOSTING PAGE=================-->

<link href="<?php echo base_url("../assets/css/hosting.css"); ?>" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!--==============Mordernizr =================-->

<script src="<?php echo base_url("../assets/js/modernizr.js"); ?>"></script>

<!--=================== Jquery STARTS=======================-->

<script src="<?php echo base_url("../assets/js/jquery.min.js"); ?>"></script>

<!--==============CONTACT FORM=================-->

<script src="<?php echo base_url("../assets/js/contact_form.js"); ?>"></script>
<script src="<?php echo base_url("../assets/js/bootstrapValidator.js"); ?>" type="text/javascript"></script>                        
<script src="<?php echo base_url("../public//plugins/validate/form_validations.js"); ?>" type="text/javascript"></script>
<link href="<?php echo base_url("../assets/css/bootstrapValidator.css"); ?>" rel="stylesheet" type="text/css" />



<!--===================FLEX SLIDER STARTS=======================-->

<link rel="stylesheet" href="<?php echo base_url("../assets/css/flexslider.css"); ?>" />
<script src="<?php echo base_url("../assets/js/jquery.min.js"); ?>"></script>
<script src="<?php echo base_url("../assets/js/jquery.flexslider.js"); ?>"></script>
<script type="text/javascript">
  $(window).load(function() {
     $('.flexslider').flexslider({
        animation: "slide",
    useCSS: Modernizr.touch
      });
  });
</script>

<!--===================FLEX SLIDER ENDS=======================-->

</head>
<body>
<!--==============Logo & Menu Bar=================-->

<nav class="navbar navbar-default navbar-fixed-top" role="navigation"> 
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand" href="<?= base_url("Login")?>"> <img src="<?php echo base_url("../assets/images/mining-logox.png"); ?>" alt="logo"></a> </div>
    
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
      <ul class="nav navbar-nav navbar-right">
        <?php if(isset($_SESSION['idperson'])){?>
        <li><p><?=$this->lang->line('ihead_user'); echo $_SESSION['user'];?></p></li>
        <li><a href="<?= base_url("Login")?>"><?=$this->lang->line('ihead_home');?></a></li>
        <li class="hidden-sm"><a href="<?= base_url("Expenses")?>"><?=$this->lang->line('ihead_exp');?></a></li>
        <li class="hidden-sm"><a href="<?= base_url("Expenses/getTypeExpenses")?>"><?=$this->lang->line('ihead_type');?></a></li>
        <li><a href="<?= base_url("Expenses/newTypeExpense")?>"><?=$this->lang->line('ihead_create');?></a></li>
        <li class=""><a href="<?= base_url("Login/logOut")?>" class="btn btn-danger"><?=$this->lang->line('ihead_logout');?></a></li>
        <?php } else{ ?>
        <li><a href="#" role="button" data-toggle="modal" data-target="#Login" class="btn btn-success"><?=$this->lang->line('ihead_login');?></a></li>
        <?php }?>
        <li class="dropdown icon-flag"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-flag-o fa-lg"></i><?=$this->lang->line('ihead_lang');?><b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo base_url("Init/index?lang=english"); ?>"><?=$this->lang->line('ihead_usa');?></a></li>
            <li><a href="<?php echo base_url("Init/index?lang=spanish"); ?>"><?=$this->lang->line('ihead_spain');?></a></li>
          </ul>
        </li>
      </ul>
      </li>
      </ul>
    </div>
    <!-- /.navbar-collapse --> 
    
  </div>
</nav>