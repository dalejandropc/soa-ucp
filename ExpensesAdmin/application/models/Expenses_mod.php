<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	/**
	* 
	*/
	class Expenses_mod extends CI_Model
	{
		function __construct(){
			parent::__construct();
			$this->load->library('Snoopy');
		}
		function view_type_expenses(){
			$ws = WSPATCH . 'typeexpenses?format=json';
			$json = file_get_contents($ws);
			$replace = array('\\');
			$json = str_replace($replace, '', $json);
			$json = substr($json, 2);
			$json = substr($json, 0, -2);
			$json = '['.$json.']';
			$data = json_decode($json);
			return $data;
		}
		function view_expenses(){
			$ws = WSPATCH . 'getexpenses?format=json';
			$json = file_get_contents($ws);
			$replace = array('\\');
			$json = str_replace($replace, '', $json);
			$json = substr($json, 2);
			$json = substr($json, 0, -2);
			$json = '['.$json.']';
			$data = json_decode($json);
			return $data;
		}
		function newTypeExpense($nameus, $namept, $namesp){
			$snoop = new Snoopy;
			$submit_url = WSPATCH . 'newtypeexpense?format=json';
    
		    $submit_vars["nameus"] = $nameus;
		    $submit_vars["namept"] = $namept;
		    $submit_vars["namesp"] = $namesp;
		    
		    if($snoop->submit($submit_url,$submit_vars)){
		        return 1;
		        //También puedes obtener el resultado del proceso
		        //en la variable $snoopy->results;
		    }else{
	        	// Y si no se pudo... muestras el error:
	       		//echo "error: ".$snoop;
	       		return 0;
	       	}
		}
		function login($user, $pass){
			$ws = WSPATCH . 'login?format=json&user=' . $user . '&password=' . $pass;
			$json = file_get_contents($ws);
			$replace = array('\\');
			$json = str_replace($replace, '', $json);
			$json = substr($json, 2);
			$json = substr($json, 0, -2);
			$data = json_decode($json);
			if($data){
				foreach ($data as $key => $value) {
					$_SESSION[$key] = $value;
				}
			}
		}
	}
?>