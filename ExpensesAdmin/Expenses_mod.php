<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	/**
	* 
	*/
	class Expenses_mod extends CI_Model
	{
		function __construct(){
			parent::__construct();
			$this->load->library('Snoopy');
		}
		function view_type_expenses(){
			$ws = WSPATCH . 'typeexpenses?format=json';
			return file_get_contents($ws);
		}
		function view_expenses(){
			$ws = WSPATCH . 'getexpenses?format=json';
			return file_get_contents($ws);
		}
		function newexpense($type, $person, $amount){
			$snoop = new Snoopy;
			$submit_url = WSPATCH . 'newexpense?format=json';
    
		    // Variables que vas a enviar al script:
		    $submit_vars["type"] = $type;
		    $submit_vars["person"] = $person;
		    $submit_vars["amount"] = $amount;
		    // etc, etc... con todas las variables
		    
		    if($snoop->submit($submit_url,$submit_vars)){
		        echo "todo ok!";
		        //También puedes obtener el resultado del proceso
		        //en la variable $snoopy->results;
		    }else{
	        // Y si no se pudo... muestras el error:
	       		echo "error: ".$snoop;
	       	}
			//$ws = WSPATCH . 'newexpenseformat=json';
			//return file_get_contents($ws);
		}
		function login($user, $pass){
			$ws = WSPATCH . 'login?format=json&user=' . $user . '&password=' . $pass;
			$json = file_get_contents($ws);
			//$json= json_encode($json);
			$replace = array('\\');
			$json = str_replace($replace, '', $json);
			$json = substr($json, 2);
			$json = substr($json, 0, -2);
			$data = json_decode($json);
			foreach ($data as $key => $value) {
				$_SESSION[$key] = $value;
			}
			
		}
	}
?>