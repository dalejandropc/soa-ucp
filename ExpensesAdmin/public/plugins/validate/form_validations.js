
$(document).ready(function () {

//Validacion campos Productos   
    var validacion = $("#register").bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            first_name: {
                row: '.col-xs-4',
                message: "*",
                validators: {
                    notEmpty: {
                        message: "*"
                    },                
                }

            },
            last_name: {
                message: "*",
                validators: {
                    notEmpty: {
                        message: "*"
                    },                   
                }

            },
            email: {
                message: "*",
                validators: {
                    notEmpty: {
                        message: "*"
                    },                   
                }

            },
            mobile: {
                message: "*",
                validators: {
                    notEmpty: {
                        message: "*"
                    },                   
                }

            },
            country: {
                message: "*",
                validators: {
                    notEmpty: {
                        message: "*"
                    },                   
                }

            },
            password: {
                message: "*",
                validators: {
                    notEmpty: {
                        message: "*"
                    },
                    stringLength: {
                        min: 8,
                        max: 30,
                        message: 'Password must contain at least eight characters!'
                    }//mas validaciones de producto aca                  
                }

            },
            password2: {
                message: "*",
                validators: {
                    notEmpty: {
                        message: "*"
                    },
                    stringLength: {
                        min: 8,
                        max: 30,
                        message: 'Password must contain at least eight characters!'
                    }//mas validaciones de producto aca                  
                }

            },                    
        }

    });

});
/*
 validacion.on("success.form.bv", function(event) {
 event.preventDefault();
 $("#form_productos").addClass('hidden');
 $("#confirmacion").removeClass('hidden');
 });
 
 });
 */


/*
 
 $(document).ready(function() {
 console.log('Iniciando sitio');
 
 $("#form").validate({
 rules: {
 nombre_producto: {
 required : true,
 }, 
 referencia : {
 required : true,
 }		
 },
 
 submitHandler: function(form){
 form.submit();
 
 }
 });
 });
 */