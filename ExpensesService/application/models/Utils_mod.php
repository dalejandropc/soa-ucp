<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	/**
	* 
	*/
	class Utils_mod extends CI_Model
	{
		function __construct(){
			parent::__construct();
			$this->load->database();
		}
		function getPerson($mail){
			$this->db->select('*');
		    $this->db->from('expenses.person');
		    $this->db->where(array('email'=>$mail));
		    $query = $this->db->get();
			return json_encode($query->result());			
		}
		function setPerson($data){
			$this->db->select('*');
		    $this->db->insert('expenses.person', $data);
		    $query = $this->db->get();
			return json_encode($query->result());			
		}
		function getPeople(){
			$this->db->select('*');
		    $this->db->from('expenses.person');
		    $query = $this->db->get();
			return json_encode($query->result());			
		}
		function getCountries(){
			$this->db->select('*');
		    $this->db->from('expenses.country');
		    $query = $this->db->get();
			return json_encode($query->result());			
		}
		function getRegions($Country,$languaje='sp'){
			$this->db->select('r.id, r.idcountry, c.name' . $languaje . ', r.name' . $languaje);
		    $this->db->from('expenses.region AS r');
		    $this->db->join('expenses.country AS c', 'r.idcountry = c.id');
		    $this->db->where(array('r.idcountry'=>$Country));
		    $query = $this->db->get();
			return json_encode($query->result());			
		}
		function getTypeExpense($languaje='sp'){
			$this->db->select('id, name' . $languaje);
		    $this->db->from('expenses.typeexpense');
		    $query = $this->db->get();
			return json_encode($query->result());			
		}
		function setTypeExpense($data){
		    $this->db->insert('expenses.typeexpense', $data);
		    $query = $this->db->get();
			return json_encode($query->result());			
		}
		function setExpense($data){
		    $this->db->insert('expenses.expense', $data);
		    $query = $this->db->get();
			return json_encode($query->result());			
		}
		function getExpenses($languaje = 'sp'){
			$this->db->select('email, fullname, name' . $languaje . ',amount');
		    $this->db->from('expenses.vw_expenses');
		    $query = $this->db->get();
			return json_encode($query->result());			
		}
		function getExpensesPerson($person){
			$this->db->select('*');
		    $this->db->from('expenses.expense');
		    $this->db->where(array('idperson'=>$person));
		    $query = $this->db->get();
			return json_encode($query->result());			
		}
		function login($user, $password){
			$this->db->select('*');
		    $this->db->from('expenses.vw_login');
		    $this->db->where(array('user'=>$user, 'password'=>md5($password)));
		    $query = $this->db->get();
			return json_encode($query->result());			
		}
	}
?>