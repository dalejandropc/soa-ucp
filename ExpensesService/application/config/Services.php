<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Services extends REST_Controller {

	public function __constuct(){
		parent::__constuct();

		$this->load->database();
		$this->load->helper('url');
	}
	public function person_post() {
        $this->load->model('Utils_mod');
        $this->response($this->Utils_mod->getPerson($_POST['mail']));
    }
	public function newperson_post() {
		$data = array(
			'email' => $_POST['mail'],
			'fullname' => $_POST['name'],
			'idregion' => $_POST['region'],
			'languaje' => $_POST['lang'],
			'active' => '1'
		);
        $this->load->model('Utils_mod');
        $this->response($this->Utils_mod->setPerson($data));
    }
	public function people_get() {
        $this->load->model('Utils_mod');
        $this->response($this->Utils_mod->getPeople());
    }
	public function countries_get() {
        $this->load->model('Utils_mod');
        $this->response($this->Utils_mod->getCountries());
    }
	public function regions_get() {
        $this->load->model('Utils_mod');
        $this->response($this->Utils_mod->getRegions($_GET['country']));
    }
	public function typeexpenses_get() {
        $this->load->model('Utils_mod');
        $this->response($this->Utils_mod->getTypeExpense());
    }
	public function newexpense_post() {
		$data = array(
			'idtypeexpense' => $_POST['type'],
			'idperson' => $_POST['person'],
			'amount' => $_POST['amount'],
			'active' => '1'
		);
        $this->load->model('Utils_mod');
        $this->response($this->Utils_mod->setExpense($data));
    }
	public function getexpenses_get() {
        $this->load->model('Utils_mod');
        $this->response($this->Utils_mod->getExpenses());
    }
	public function getexpensesperson_post() {
        $this->load->model('Utils_mod');
        $this->response($this->Utils_mod->getExpenses($_POST['person']));
    }

    public function dd_get() {
    $this->response([
        'returned from delete:' => '645',
    ]);
} 
}